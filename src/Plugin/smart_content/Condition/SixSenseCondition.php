<?php

namespace Drupal\smart_content_6sense\Plugin\smart_content\Condition;

use Drupal\smart_content\Condition\ConditionTypeConfigurableBase;

/**
 * Provides a 6sense condition plugin.
 *
 * @SmartCondition(
 *   id = "sixsense",
 *   label = @Translation("6sense"),
 *   group = "sixsense",
 *   deriver = "Drupal\smart_content_6sense\Plugin\Derivative\SixSenseConditionDeriver"
 * )
 */
class SixSenseCondition extends ConditionTypeConfigurableBase {

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    $libraries = array_unique(array_merge(parent::getLibraries(), ['smart_content_6sense/condition.6sense']));
    return $libraries;
  }

}
