<?php


namespace Drupal\smart_content_browser\Plugin\smart_content\Condition\Group;

use Drupal\smart_content\Condition\Group\ConditionGroupBase;

/**
 * Provides a condition group for 6sense conditions.
 *
 * @SmartConditionGroup(
 *   id = "sixsense",
 *   label = @Translation("6sense")
 * )
 */
class SixSense extends ConditionGroupBase {

}
