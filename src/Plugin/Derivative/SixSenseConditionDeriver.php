<?php

namespace Drupal\smart_content_6sense\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides condition plugin definitions for 6sense fields.
 *
 * @see Drupal\smart_content_6sense\Plugin\smart_content\Condition\SixSenseCondition
 */
class SixSenseConditionDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;


  /**
   * SixSenseConditionDeriver constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];
    $sixsense_fields = $this->getStaticFields();
    foreach ($sixsense_fields as $key => $sixsense_field) {
      $this->derivatives[$key] = $sixsense_field + $base_plugin_definition;
    }
    return $this->derivatives;
  }


  /**
   * Function to return a static list of fields as defined in 6sense's
   * firmographic attributes overview
   *
   * @return array
   *   An array of fields from 6sense.
   */
  protected function getStaticFields() {
    return [
      'domain' => [
        'label' => 'Domain',
        'type' => 'textfield',
      ],
      'name' => [
        'label' => 'Name',
        'type' => 'textfield',
      ],
      'industry' => [
        'label' => 'Industry',
        'type' => 'select',
        'options_callback' => [get_class($this), 'getIndustryOptions'],
      ],
      'city' => [
        'label' => 'City',
        'type' => 'textfield',
      ],
      'state' => [
        'label' => 'State',
        'type' => 'textfield',
      ],
      'country' => [
        'label' => 'Country',
        'type' => 'select',
        'options_callback' => [get_class($this), 'getCountryOptions'],
      ],
      'country_iso_code' => [
        'label' => 'Country ISO Code',
        'type' => 'textfield',
      ],
      'region' => [
        'label' => 'Region',
        'type' => 'select',
        'options_callback' => [get_class($this), 'getRegionOptions'],
      ],
      'address' => [
        'label' => 'Street Address',
        'type' => 'textfield',
      ],
      'zip' => [
        'label' => 'Zip Code / Postal Code',
        'type' => 'number',
      ],
      'phone' => [
        'label' => 'Company Phone',
        'type' => 'textfield',
      ],
      'sic' => [
        'label' => 'SIC Code',
        'type' => 'number',
      ],
      'sic_description' => [
        'label' => 'SIC Description',
        'type' => 'textfield',
      ],
      'naics' => [
        'label' => 'NAICs Code',
        'type' => 'number',
      ],
      'naics_description' => [
        'label' => 'NAICs Description',
        'type' => 'number',
      ],
      'employee_count' => [
        'label' => 'Employee Count',
        'type' => 'number',
      ],
      'employee_range' => [
        'label' => 'Employee Range',
        'type' => 'select',
        'options_callback' => [get_class($this), 'getEmployeeRangeOptions'],
      ],
      'annual_revenue' => [
        'label' => 'Annual Revenue',
        'type' => 'number',
        'format_options' => [
          'prefix' => '$',
        ],
      ],
      'revenue_range' => [
        'label' => 'Revenue Range',
        'type' => 'select',
        'options_callback' => [get_class($this), 'getRevenueRangeOptions'],
      ],
      'segments' => [
        'label' => 'Segments (User-Defined)',
        'type' => 'array_textfield',
      ],
      'profile_fit' => [
        'label' => 'Profile Fit (if predictive customer)',
        'type' => 'textfield',
      ],
      'buying_stage' => [
        'label' => 'Buying Stage (if predictive customer)',
        'type' => 'textfield',
      ],
      'profile_score' => [
        'label' => 'Profile Score (if predictive customer)',
        'type' => 'number',
      ],
      'intent_score' => [
        'label' => 'Intent Score (if predictive customer)',
        'type' => 'number',
      ],
    ];
  }

  /**
   * Returns list of 'Operating Systems' for select element.
   *
   * @return array
   *   Array of Operation Systems.
   */
  public static function getIndustryOptions() {
    return [
      'Aerospace and Defense' => 'Aerospace and Defense',
      'Agriculture' => 'Agriculture',
      'Apparel' => 'Apparel',
      'Associations' => 'Associations',
      'Automotive' => 'Automotive',
      'Biotech' => 'Biotech',
      'Business Services' => 'Business Services',
      'Construction' => 'Construction',
      'Consumer Goods and Services' => 'Consumer Goods and Services',
      'Education' => 'Education',
      'Energy and Utilities' => 'Energy and Utilities',
      'Financial Services' => 'Financial Services',
      'Food and Beverage' => 'Food and Beverage',
      'Furniture' => 'Furniture',
      'Government' => 'Government',
      'Hardware' => 'Hardware',
      'Healthcare and Medical' => 'Healthcare and Medical',
      'Home and Garden' => 'Home and Garden',
      'Hospitality and Travel' => 'Hospitality and Travel',
      'Manufacturing' => 'Manufacturing',
      'Media and Entertainment' => 'Media and Entertainment',
      'Mining' => 'Mining',
      'Pharmaceuticals' => 'Pharmaceuticals',
      'Printing and Publishing' => 'Printing and Publishing',
      'Real Estate' => 'Real Estate ',
      'Recreation' => 'Recreation',
      'Retail and Distribution' => 'Retail and Distribution',
      'Software and Technology' => 'Software and Technology',
      'Telecommunications' => 'Telecommunications',
      'Textiles' => 'Textiles',
      'Transportation and Logistics' => 'Transportation and Logistics',
    ];
  }

  /**
   * Returns list of 'Operating Systems' for select element.
   *
   * @return array
   *   Array of Operation Systems.
   */
  public static function getBuyingStageOptions() {
    return [
      'Awareness' => 'Awareness',
      'Consideration' => 'Consideration',
      'Decision' => 'Decision',
      'Purchase' => 'Purchase',
    ];
  }

  /**
   * Returns list of 'Operating Systems' for select element.
   *
   * @return array
   *   Array of Operation Systems.
   */
  public static function getProfileFitOptions() {
    return [
      'Strong' => 'Strong',
      'Moderate' => 'Moderate',
      'Weak' => 'Weak',
    ];
  }

  /**
   * Returns list of 'Operating Systems' for select element.
   *
   * @return array
   *   Array of Operation Systems.
   */
  public static function getEmployeeRangeOptions() {
    return [
      '0 - 9' => '0 - 9',
      '10 - 19' => '10 - 19',
      '20 - 49' => '20 - 49',
      '50 - 99' => '50 - 99',
      '100 - 249' => '100 - 249',
      '250 - 499' => '250 - 499',
      '500 - 999' => '500 - 999',
      '1,000 - 4,999' => '1,000 - 4,999',
      '5,000 - 9,999' => '5,000 - 9,999',
      '10,000+' => '10,000+',
    ];
  }

  /**
   * Returns list of 'Operating Systems' for select element.
   *
   * @return array
   *   Array of Operation Systems.
   */
  public static function getCountryOptions() {
    $options = [
      'Afghanistan',
      'Aland Islands',
      'Albania',
      'Algeria',
      'American Samoa',
      'Andorra',
      'Angola',
      'Anguilla',
      'Antarctica',
      'Antigua and Barbuda',
      'Argentina',
      'Armenia',
      'Aruba',
      'Australia',
      'Austria',
      'Azerbaijan',
      'Bahamas',
      'Bahrain',
      'Bangladesh',
      'Barbados',
      'Belarus',
      'Belgium',
      'Belize',
      'Benin',
      'Bermuda',
      'Bhutan',
      'Bolivia',
      'Bonaire, Saint Eustatius and Saba',
      'Bosnia and Herzegovina',
      'Botswana',
      'Bouvet Island',
      'Brazil',
      'British Indian Ocean Territory',
      'British Virgin Islands',
      'Brunei',
      'Bulgaria',
      'Burkina Faso',
      'Burundi',
      'Cambodia',
      'Cameroon',
      'Canada',
      'Cape Verde',
      'Cayman Islands',
      'Central African Republic',
      'Chad',
      'Chile',
      'Christmas Island',
      'Cocos Islands',
      'Colombia',
      'Comoros',
      'Cook Islands',
      'Costa Rica',
      'Croatia',
      'Cuba',
      'Curacao',
      'Cyprus',
      'Czech Republic',
      'Democratic Republic of the Congo',
      'Denmark',
      'Djibouti',
      'Dominica',
      'Dominican Republic',
      'East Timor',
      'Ecuador',
      'Egypt',
      'El Salvador',
      'Equatorial Guinea',
      'Eritrea',
      'Estonia',
      'Ethiopia',
      'Falkland Islands',
      'Faroe Islands',
      'Fiji',
      'Finland',
      'France',
      'French Guiana',
      'French Polynesia',
      'French Southern Territories',
      'Gabon',
      'Gambia',
      'Georgia',
      'Germany',
      'Ghana',
      'Gibraltar',
      'Greece',
      'Greenland',
      'Grenada',
      'Guadeloupe',
      'Guam',
      'Guatemala',
      'Guernsey',
      'Guinea',
      'Guinea-Bissau',
      'Guyana',
      'Haiti',
      'Heard Island and McDonald Islands',
      'Honduras',
      'Hong Kong',
      'Hungary',
      'Iceland',
      'India',
      'Indonesia',
      'Iran',
      'Iraq',
      'Ireland',
      'Isle of Man',
      'Israel',
      'Italy',
      'Ivory Coast',
      'Jamaica',
      'Japan',
      'Jersey',
      'Jordan',
      'Kazakhstan',
      'Kenya',
      'Kiribati',
      'Korea',
      'Kosovo',
      'Kuwait',
      'Kyrgyzstan',
      'Laos',
      'Latvia',
      'Lebanon',
      'Lesotho',
      'Liberia',
      'Libya',
      'Liechtenstein',
      'Lithuania',
      'Luxembourg',
      'Macao',
      'Macedonia',
      'Madagascar',
      'Malawi',
      'Malaysia',
      'Maldives',
      'Mali',
      'Malta',
      'Marshall Islands',
      'Martinique',
      'Mauritania',
      'Mauritius',
      'Mayotte',
      'Mexico',
      'Micronesia',
      'Moldova',
      'Monaco',
      'Mongolia',
      'Montenegro',
      'Montserrat',
      'Morocco',
      'Mozambique',
      'Myanmar',
      'Namibia',
      'Nauru',
      'Nepal',
      'Netherlands',
      'Netherlands Antilles',
      'New Caledonia',
      'New Zealand',
      'Nicaragua',
      'Niger',
      'Nigeria',
      'Niue',
      'Norfolk Island',
      'North Korea',
      'Northern Mariana Islands',
      'Norway',
      'Oman',
      'Pakistan',
      'Palau',
      'Palestinian Territory',
      'Panama',
      'Papua New Guinea',
      'Paraguay',
      "People's Republic of China",
      'Peru',
      'Philippines',
      'Pitcairn',
      'Poland',
      'Portugal',
      'Puerto Rico',
      'Qatar',
      'Republic of the Congo',
      'Reunion',
      'Romania',
      'Russia',
      'Rwanda',
      'Saint Barthelemy',
      'Saint Helena',
      'Saint Kitts and Nevis',
      'Saint Lucia',
      'Saint Martin',
      'Saint Pierre and Miquelon',
      'Saint Vincent and the Grenadines',
      'Samoa',
      'San Marino',
      'Sao Tome and Principe',
      'Saudi Arabia',
      'Senegal',
      'Serbia',
      'Serbia and Montenegro',
      'Seychelles',
      'Sierra Leone',
      'Singapore',
      'Sint Maarten',
      'Slovakia',
      'Slovenia',
      'Solomon Islands',
      'Somalia',
      'South Africa',
      'South Georgia and the South Sandwich Islands',
      'South Sudan',
      'Spain',
      'Sri Lanka',
      'Sudan',
      'Suriname',
      'Svalbard and Jan Mayen',
      'Swaziland',
      'Sweden',
      'Switzerland',
      'Syria',
      'Taiwan',
      'Tajikistan',
      'Tanzania',
      'Thailand',
      'Togo',
      'Tokelau',
      'Tonga',
      'Trinidad and Tobago',
      'Tunisia',
      'Turkey',
      'Turkmenistan',
      'Turks and Caicos Islands',
      'Tuvalu',
      'U.S. Virgin Islands',
      'Uganda',
      'Ukraine',
      'United Arab Emirates',
      'United Kingdom',
      'United States',
      'United States Minor Outlying Islands',
      'Uruguay',
      'Uzbekistan',
      'Vanuatu',
      'Vatican',
      'Venezuela',
      'Vietnam',
      'Wallis and Futuna',
      'Western Sahara',
      'Yemen',
      'Zambia',
      'Zimbabwe',
    ];
    return array_combine($options, $options);
  }

  /**
   * Returns list of 'Operating Systems' for select element.
   *
   * @return array
   *   Array of Operation Systems.
   */
  public static function getRegionOptions() {
    return [
      'Australia and New Zealand' => 'Australia and New Zealand',
      'Caribbean' => 'Caribbean',
      'Central America' => 'Central America',
      'Central Asia' => 'Central Asia',
      'Eastern Africa' => 'Eastern Africa',
      'Eastern Asia' => 'Eastern Asia',
      'Eastern Europe' => 'Eastern Europe',
      'Melanesia' => 'Melanesia',
      'Micronesia' => 'Micronesia',
      'Middle Africa' => 'Middle Africa',
      'Northern Africa' => 'Northern Africa',
      'Northern America' => 'Northern America',
      'Northern Europe' => 'Northern Europe',
      'Polynesia' => 'Polynesia',
      'South America' => 'South America',
      'South Eastern Asia' => 'South Eastern Asia',
      'Southern Africa' => 'Southern Africa',
      'Southern Asia' => 'Southern Asia',
      'Southern Europe' => 'Southern Europe',
      'Western Africa' => 'Western Africa',
      'Western Asia' => 'Western Asia',
      'Western Europe' => 'Western Europe',
    ];
  }

  /**
   * Returns list of 'Operating Systems' for select element.
   *
   * @return array
   *   Array of Operation Systems.
   */
  public static function getRevenueRangeOptions() {
    return [
      '$1 - $1M' => '$1 - $1M',
      '$1M - $5M' => '$1M - $5M',
      '$5M - $10M' => '$5M - $10M',
      '$10M - $25M' => '$10M - $25M',
      '$25M - $50M' => '$25M - $50M',
      '$50M - $100M' => '$50M - $100M',
      '$100M - $250M' => '$100M - $250M',
      '$250M - $500M' => '$250M - $500M',
      '$500M - $1B' => '$500M - $1B',
      '$1B - $2B' => '$1B - $2B',
      '$2.5B - $5B' => '$2.5B - $5B',
      '$5B+' => '$5B+',
    ];
  }

}
