(function (Drupal) {
  Drupal.smartContent = Drupal.smartContent || {};
  Drupal.smartContent.plugin = Drupal.smartContent.plugin || {};
  Drupal.smartContent.plugin.Field = Drupal.smartContent.plugin.Field || {};

  Drupal.smartContent.plugin.Field['sixsense'] = function (condition) {
    let key = condition.field.pluginId.split(':')[1];
    let companyDetails = localStorage.getItem('_6senseCompanyDetails');
    if(companyDetails) {
      Drupal.smartContent.sixsense = JSON.parse(companyDetails);
    }
    else {
      Drupal.smartContent.sixsense = new Promise((resolve, reject) => {
        let attempts = 0;
        // todo: Give the interval more time to complete, but still resolve early
        //   if delayed too long.
        const interval = setInterval(() => {
          if (attempts < 200) {
            companyDetails = localStorage.getItem('_6senseCompanyDetails');
            if (companyDetails) {
              clearInterval(interval);
              resolve(JSON.parse(companyDetails));
            }
          }
          else {
            clearInterval(interval);
            resolve({});
          }
          attempts++;
        }, 20);
      });
    }

    const get = (p, o) =>
      p.reduce((xs, x) => (xs && xs[x]) ? xs[x] : null, o)

    return Promise.resolve(Drupal.smartContent.sixsense).then((value) => {
      return get(['company', key], value) || get([key], value) || get(['scores', 'sales_performance_management', key], value)
    });
  }

}(Drupal));
